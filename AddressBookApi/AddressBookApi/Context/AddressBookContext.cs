﻿using System.Data.Entity;
using AddressBookApi.Models;

namespace AddressBookApi.Context
{
    public class AddressBookContext : DbContext
    {
        public AddressBookContext() : base("AddressBookConnection") { }

        public DbSet<Person> People { get; set; }
        public DbSet<Address> Addresses { get; set; }
        public DbSet<City> Cities { get; set; }
        public DbSet<PostCode> PostCodes { get; set; }
        public DbSet<Phone> Phones { get; set; }
        public DbSet<EAddress> EAddresses { get; set; }
        public DbSet<Social> Socials { get; set; }

    }
}