﻿using System;
using System.Data.Entity;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AddressBookApi.Models;

namespace AddressBookApi.Context
{
    public class DatabaseInitializer : DropCreateDatabaseIfModelChanges<AddressBookContext>
    {
        protected override void Seed(AddressBookContext context)
        {
            base.Seed(context);

            var people = new List<Person> {
                new Person
                {
                    FirstName = "Dimitar",
                    LastName = "Daskalov",
                    Address = new Address
                                {
                                    AddressLineOne = "14 MyStreet Str.",
                                    AddressLineTwo = "2nd fl.,apt 4",
                                    City = new City { Name = "Maina Town" },
                                    PostCode = new PostCode { Postcode = "4000" }
                                },
                    Phone = new Phone
                                {
                                    HomeNumber = "032547985",
                                    BusinessNumber = "0897456321",
                                    Mobile = "0883335566"
                                },
                    EAddress = new EAddress
                                {
                                    Email ="mitko@myweb.me",
                                    Webpage ="www.myweb.me"
                                },
                    Social= new Social
                                {
                                    FacebookUrl ="www.facebook.com",
                                    LinkedInUrl ="linkedin.com"
                                }
                },
                new Person
                    {
                        FirstName = "Petya",
                        LastName = "Ivanova",
                        Address = new Address
                                    {
                                        AddressLineOne = "14 HerStreet Str.",
                                        AddressLineTwo = "1st fl.,apt 1",
                                        City = new City { Name = "Varna" },
                                        PostCode = new PostCode { Postcode = "9000" }
                                    },
                        Phone = new Phone
                                    {
                                        HomeNumber = "032123456",
                                        BusinessNumber = "0888776655",
                                        Mobile = "0885335566"
                                    },
                        EAddress = new EAddress
                                    {
                                        Email ="pepa@myweb.me",
                                        Webpage ="www.herweb.me"
                                    },
                        Social= new Social
                                {
                                    FacebookUrl ="www.facebook.com",
                                    LinkedInUrl ="linkedin.com"
                                }
                    }
            };

            context.People.AddRange(people);

            context.SaveChanges();

        }
    }
}