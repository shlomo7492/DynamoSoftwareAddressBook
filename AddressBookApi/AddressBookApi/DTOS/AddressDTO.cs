﻿namespace AddressBookApi.DTOS
{
    public class AddressDTO
    {
        public int ID { get; set; }

        public string AddressLineOne { get; set; }
        public string AddressLineTwo { get; set; }

        public string City { get; set; }

        public string PostCode { get; set; }
    }
}