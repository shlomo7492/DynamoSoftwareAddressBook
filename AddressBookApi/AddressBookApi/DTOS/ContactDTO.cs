﻿using AddressBookApi.Models;

namespace AddressBookApi.DTOS
{
    public class ContactDTO
    {
        public int ID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }


        public AddressDTO Address { get; set; }

        public Phone Phone { get; set; }

        public EAddress EAddress { get; set; }

        public Social Social { get; set; }
    }
}