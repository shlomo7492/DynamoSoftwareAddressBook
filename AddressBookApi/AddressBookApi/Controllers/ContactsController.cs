﻿using AddressBookApi.Context;
using AddressBookApi.DTOS;
using AddressBookApi.Models;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;

namespace AddressBookApi.Controllers
{
    public class ContactsController : ApiController
    {
        // initialize new context instance
        AddressBookContext db = new AddressBookContext();
        
        // GET: api/Contacts
        public IEnumerable<ContactDTO> Get()
        {
            //Initializing colections of entities to help with retrieving the data
            List<ContactDTO> contacts = new List<ContactDTO>();
            List <AddressDTO> addresses = new List<AddressDTO>();
            List<City> cities = new List<City>();
            List<PostCode> postCodes = new List<PostCode>();

            List<Phone> phones = new List<Phone>();
            List<EAddress> eAddresses = new List<EAddress>();
            List<Social> socials = new List<Social>();

            using (db)
            {
                //Populating the postCodes collection
                foreach (var postcode in db.PostCodes)
                {
                    postCodes.Add(postcode);
                }

                //Populating the cities collection
                foreach (var city in db.Cities)
                {
                    cities.Add(city);
                }

                //Populating the phones collection
                foreach (var phone in db.Phones)
                {
                    phones.Add(phone);
                }

                //Populating the eAddresses collection
                foreach (var eAddress in db.EAddresses)
                {
                    eAddresses.Add(eAddress);
                }

                //Populating the socials collection
                foreach (var social in db.Socials)
                {
                    socials.Add(social);
                }

                //Creating each AddressDTO object and populating the addresses collection
                foreach (var record in db.Addresses)
                {
                    AddressDTO address = new AddressDTO();

                    address.ID = record.AddressID;
                    address.AddressLineOne = record.AddressLineOne;
                    address.AddressLineTwo = record.AddressLineTwo;
                    address.PostCode = postCodes.Where(pc=>pc.PostCodeID == record.PostCodeID)
                                                .SingleOrDefault()
                                                .Postcode;
                    address.City = cities.Where(c => c.CityID == record.CityID)
                                        .SingleOrDefault()
                                        .Name;

                    addresses.Add(address);
                }

                //reading all the records of People table and using all the collections above to populate 
                //final list of ContactDTO objects
                foreach (var record in db.People)
                {
                    ContactDTO contact = new ContactDTO();

                    contact.ID = record.PersonID;
                    contact.FirstName = record.FirstName;
                    contact.LastName = record.LastName;
                    contact.Address = addresses.Where(a => a.ID == record.AddressID)
                                               .SingleOrDefault();
                    contact.Phone = phones.Where(ph => ph.PhoneID == record.PhoneID)
                                          .SingleOrDefault();
                    contact.EAddress = eAddresses.Where(ea => ea.EAddressID == record.EAddressID)
                                                 .SingleOrDefault();
                    contact.Social = socials.Where(so => so.SocialID == record.SocialID)
                                            .SingleOrDefault();

                    contacts.Add(contact);
                }              
            }
            
            return contacts;
        }

        // GET: api/Contacts/5
        public ContactDTO Get(int id)
        {
            //Initializing the return type object
            ContactDTO contact = new ContactDTO();
            AddressDTO address = new AddressDTO();

            //retrieving the record with specified id from Peoples table
            var person = db.People
                           .Where(p => p.PersonID == id)
                           .FirstOrDefault();

            //retrieving the address record coresponding to the person with the given id, as well as 
            //the city and posctode for this address
            var tempAddress = db.Addresses
                                .Where(addr => addr.AddressID == person.AddressID)
                                .SingleOrDefault();
            var city = db.Cities
                         .Where(c => c.CityID == tempAddress.CityID)
                         .SingleOrDefault()
                         .Name;
            var postCode = db.PostCodes
                             .Where(pc => pc.PostCodeID == tempAddress.PostCodeID)
                             .SingleOrDefault()
                             .Postcode;

            //Upating the address properties valuies
            address.AddressLineOne = tempAddress.AddressLineOne;
            address.AddressLineTwo = tempAddress.AddressLineTwo;
            address.PostCode = postCode;
            address.City = city;

            //Updating the data in the return variable
            contact.ID = person.PersonID;
            contact.FirstName = person.FirstName;
            contact.LastName = person.LastName;
            contact.Address = address;
            contact.Phone = db.Phones
                              .Where(ph => ph.PhoneID == person.PhoneID)
                              .SingleOrDefault();
            contact.EAddress = db.EAddresses
                                 .Where(ea => ea.EAddressID == person.EAddressID)
                                 .SingleOrDefault();
            contact.Social = db.Socials
                               .Where(s => s.SocialID == person.SocialID)
                               .SingleOrDefault();
            
            return contact;
        }

        // POST: api/Contacts
        public void Post([FromBody]ContactDTO person)
        {
            
            db.People.Add(new Person
            {
                FirstName = person.FirstName,
                LastName = person.LastName,
                Address = new Address
                {
                    AddressLineOne = person.Address.AddressLineOne,
                    AddressLineTwo = person.Address.AddressLineTwo,
                    City = new City { Name = person.Address.City },
                    PostCode = new PostCode { Postcode = person.Address.PostCode }
                },
                Phone = new Phone
                {
                    HomeNumber = person.Phone.HomeNumber,
                    BusinessNumber = person.Phone.BusinessNumber,
                    Mobile = person.Phone.Mobile
                },
                EAddress = new EAddress
                {
                    Email = person.EAddress.Email,
                    Webpage = person.EAddress.Webpage
                },
                Social = new Social
                            {
                                FacebookUrl = person.Social.FacebookUrl,
                                TwitterUrl =person.Social.TwitterUrl,
                                InstagramUrl = person.Social.InstagramUrl,
                                LinkedInUrl = person.Social.LinkedInUrl
                            }
            });
            db.SaveChanges();
        }

        // PUT: api/Contacts/5
        public void Put(int id, [FromBody]ContactDTO contact)
        {
            /*To make City, PostCode, Address unique with other words, only one City "Plovdiv" in the Cities table
            For now there is a flaw in the DB model. You can add multiple records in Address,City and PostCode tables
            with the same info, that will differ only by ID. There has to be corection to the model, where
            Address has many people (a family lives on one addres), PostCode has many addresses, but one City
            and City got many PostCodes... */

            //Initialize some help id variables
            int addressID = 0;
            int cityID = 0;
            int postCodeID = 0;
            int phoneID = 0;
            int eAddressID = 0;
            int socialID = 0;

            //Updating some of the vlues of id variables, as well as firstName and lastName in People table
            foreach (var record in db.People)
            {
                
                if(record.PersonID == id)
                {
                    record.FirstName = contact.FirstName;
                    record.LastName = contact.LastName;

                    addressID = record.AddressID;
                    phoneID = record.PhoneID;
                    eAddressID = record.EAddressID;
                    socialID = record.SocialID;
                }
            }

            //Updating the address LineOne and LineTwo, as well as some of the help id variables
            foreach (var record in db.Addresses)
            {
                if(record.AddressID == addressID)
                {
                   record.AddressLineOne = contact.Address.AddressLineOne;
                   record.AddressLineTwo = contact.Address.AddressLineTwo;
                   cityID = record.CityID;
                   postCodeID = record.PostCodeID;
                }
            }
            //Updating city name for the specified cityID variable. Same for the postCodeID,phoneID, etc.
            foreach (var record in db.Cities)
            {
                if (record.CityID == cityID)
                {
                    record.Name = contact.Address.City;
                }
            }

            foreach (var record in db.PostCodes)
            {
                if (record.PostCodeID == postCodeID)
                {
                    record.Postcode = contact.Address.PostCode;
                }
            }

            foreach (var record in db.Phones)
            {
                if (record.PhoneID == phoneID)
                {
                    record.HomeNumber = contact.Phone.HomeNumber;
                    record.BusinessNumber = contact.Phone.BusinessNumber;
                    record.Mobile = contact.Phone.Mobile;
                }
            }

            foreach (var record in db.EAddresses)
            {
                if (record.EAddressID == eAddressID)
                {
                    record.Email = contact.EAddress.Email;
                    record.Webpage = contact.EAddress.Webpage;
                }
            }

            foreach (var record in db.Socials)
            {
                if (record.SocialID == socialID)
                {
                    record.FacebookUrl = contact.Social.FacebookUrl;
                    record.TwitterUrl = contact.Social.TwitterUrl;
                    record.InstagramUrl = contact.Social.InstagramUrl;
                    record.LinkedInUrl = contact.Social.LinkedInUrl;
                }
            }

            db.SaveChanges();
        }

        // DELETE: api/Contacts/5
        public void Delete(int id)
        {
            var contact = db.People.Where(p => p.PersonID == id)
                                   .SingleOrDefault();

            db.Entry(contact).State = System.Data.Entity.EntityState.Deleted;
            db.SaveChanges();
        }
    }
}
