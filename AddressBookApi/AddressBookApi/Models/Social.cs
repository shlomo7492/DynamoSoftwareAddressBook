﻿using System.ComponentModel.DataAnnotations;

namespace AddressBookApi.Models
{
    public class Social
    {
        [Key]
        public int SocialID { get; set; }

        public string FacebookUrl { get; set; }

        public string TwitterUrl { get; set; }

        public string InstagramUrl { get; set; }

        public string LinkedInUrl { get; set; }

    }
}