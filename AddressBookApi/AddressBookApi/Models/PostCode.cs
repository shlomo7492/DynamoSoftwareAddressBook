﻿using System.ComponentModel.DataAnnotations;

namespace AddressBookApi.Models
{
    public class PostCode
    {
        [Key]
        public int PostCodeID { get; set; }

        public string Postcode { get; set; }
    }
}