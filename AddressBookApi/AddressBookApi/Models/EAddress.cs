﻿using System.ComponentModel.DataAnnotations;

namespace AddressBookApi.Models
{
    public class EAddress
    {
        [Key]
        public int EAddressID { get; set; }

        [EmailAddress]
        public string Email { get; set; }

        public string Webpage { get; set; }
    }
}