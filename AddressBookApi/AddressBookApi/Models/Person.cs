﻿using System.ComponentModel.DataAnnotations;

namespace AddressBookApi.Models
{
    public class Person
    {
        [Key]
        public int PersonID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }

        public int AddressID { get; set; }
        public Address Address { get; set; }

        public int PhoneID { get; set; }
        public Phone Phone { get; set; }

        public int EAddressID { get; set; }
        public EAddress EAddress { get; set; }

        public int SocialID { get; set; }
        public Social Social { get; set; }
    }
}