﻿using System.ComponentModel.DataAnnotations;

namespace AddressBookApi.Models
{
    public class Address
    {
        [Key]
        public int AddressID { get; set; }

        public string AddressLineOne { get; set; }
        public string AddressLineTwo { get; set; }

        public int CityID { get; set; }
        public City City { get; set; }

        public int PostCodeID { get; set; }
        public PostCode PostCode { get; set; }

    }
}