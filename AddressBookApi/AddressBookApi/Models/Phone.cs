﻿using System.ComponentModel.DataAnnotations;

namespace AddressBookApi.Models
{
    public class Phone
    {
        [Key]
        public int PhoneID { get; set; }

        public string HomeNumber { get; set; }
        public string BusinessNumber { get; set; }
        public string Mobile { get; set; }
    }
}